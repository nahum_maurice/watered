const express = require("express");
const app = express();

//Launching the databases...
const { db_connection } = require("./services/db_management");
db_connection();

// Requiring the three different api components
// user, circle, content and financial
const user = require("./routes/user");
const light = require("./routes/light");
const post = require("./routes/post");
const circle = require("./routes/circle");

app.use(express.json()); //parse json

// Using theses component in this Core API
app.use("/users", user);
app.use("/lights", light);
app.use("/posts", post);
app.use("/circles", circle);

// Setting the external routes (Where the frontend is
// connecting in reality)

// Link to the User API
app.route("/users").get(user);
app.route("/users/create-user").post(user);
app.route("/users/get-user").get(user);
app.route("/users/get-user-profile").get(user);
app.route("/users/get-user-partial-profile").get(user);
app.route("/users/update-user").put(user);
app.route("/users/delete-user").delete(user);
app.route("/users/suspend-user").put(user);
app.route("/users/unsuspend-user").put(user);

// Link to the Light API
app.route("/lights/create-light").post(light);
app.route("/lights/get-light").get(light);
app.route("/lights/delete-light").delete(light);

// Link to the Post API
app.route("/posts/create-post").post(post);
app.route("/posts/get-post").get(post);
app.route("/posts/get-post-profile").get(post);
app.route("/posts/update-post").put(post);
app.route("/posts/delete-post").delete(post);

// Link to the Circle API
app.route("/circles/create-circle").post(circle);
app.route("/circles/get-circle").get(circle);
app.route("/circles/get-circle-profile").get(circle);
app.route("/circles/get-circle-partial-profile").get(circle);
app.route("/circles/update-circle").put(circle);
app.route("/circles/delete_circle").delete(circle);
app.route("/circles/add-member").put(circle); // invite new member
app.route("/circles/remove-member").put(circle);
app.route("/circles/add-post").put(circle);
app.route("/circles/remove-post").put(circle);

// Launching the server...
app.listen(5000, () => {
  console.log("Server started...");
});

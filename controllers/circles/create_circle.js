const { insert_one } = require("../../services/db_management");
const Circle = require("../../objects/circle");

async function create_circle(req, res, next) {
  const circle_info = req.body;

  //Ensuring that the boby is safe...
  const circle_info_mandatory = ["name", "description", "creator", "isPrivate"];
  circle_info_mandatory.forEach((prop) => {
    if (!(prop in circle_info)) {
      return res.status(406).json({
        success: false,
        msg: "The required circle information are not provided.",
      });
    }
  });

  // Creating a new circle object...
  const circle = new Circle(circle_info);

  // Adding the new created circle to the DB (using DB management service)
  result = await insert_one(circle, "circles", "main");
  if (result == true) {
    res
      .status(200)
      .json({ success: true, msg: "The new circle is successfully created" });
  } else {
    res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
  }
  next();
}

module.exports = { create_circle };

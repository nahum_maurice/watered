const { delete_one } = require("../../services/db_management");

async function delete_circle(req, res, next) {
  const circle_id_object = req.body;

  try {
    const result = await delete_one(circle_id_object, "circles", "main");
    if (result.deleted >= 1) {
      return res.status(200).json({
        success: true,
        msg: "The circle has been deleted successfully.",
      });
    } else if (result.found >= 1 && result.deleted == 0) {
      return res.status(500).json({
        success: false,
        msg: "The circle data has been found but not deleted.",
      });
    } else {
      return res
        .status(500)
        .json({ success: false, msg: "The circle has not been found." });
    }
  } catch (error) {
    return res.status(500).json({
      succes: false,
      msg: "An error occured in the server, the operation is aborted.",
    });
  }
  next();
}

module.exports = { delete_circle };

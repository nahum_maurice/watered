const { find_one } = require("../../services/db_management");

async function read_circle(req, res, next) {
  const circle_id_object = req.body;

  try {
    const existed_circle = await find_one(circle_id_object, "circles", "main");
    if (existed_circle.length !== 0) {
      circle_object = {
        name: existed_circle.name,
        description: existed_circle.description,
        creator: existed_circle.creator,
        isPrivate: existed_circle.isPrivate,
        members: existed_circle.members,
        documents: existed_circle.documents,
        creation_time: existed_circle.creation_time,
      };
      return res.status(200).json({ success: true, data: circle_object });
    } else {
      return res
        .status(404)
        .json({ success: false, msg: "The circle is not found" });
    }
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
  }
  next();
}

async function read_circle_profile(req, res, next) {
  const circle_id_object = req.body;

  try {
    const existed_circle = await find_one(circle_id_object, "circles", "main");
    if (existed_circle.length !== 0) {
      circle_object = {
        name: existed_circle.name,
        description: existed_circle.description,
        isPrivate: existed_circle.isPrivate,
        number_members: existed_circle.members.length, //To show the number of users
      };
      return res.status(200).json({ success: true, data: circle_object });
    } else {
      return res
        .status(404)
        .json({ success: false, msg: "The circle is not found" });
    }
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
  }
  next();
}

async function read_circle_partial_profile(req, res, next) {
  const circle_id_object = req.body; //Should be an object

  try {
    const existed_circle = await find_one(circle_id_object, "circles", "main");
    if (existed_circle.length !== 0) {
      circle_object = {
        name: existed_circle.name,
        isPrivate: existed_circle.isPrivate,
      };
      return res.status(200).json({ success: true, data: circle_object });
      next();
    } else {
      return res
        .status(404)
        .json({ success: false, msg: "The circle is not found" });
      next();
    }
  } catch (error) {
    return res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
    next();
  }
}

module.exports = {
  read_circle,
  read_circle_profile,
  read_circle_partial_profile,
};

const { update_one } = require("../../services/db_management");

async function update_circle(req, res, next) {
  // Verify that circle has the auth

  //They have to be like {_id: "fd9r3jeinff", data: {password: "fieief"}}
  const circle_id = req.body._id;
  const new_elements = req.body.data;

  //Checking that the data are not members nor documents...
  if ("members" in req.body || "documents" in req.body) {
    return res
      .status(406)
      .json(
        "This is not an acceptable set of parameters. Read the documentation"
      );
    next();
  }

  try {
    const result = update_one(circle_id, new_elements, "circles", "main");
    if ((await result).modified >= 1) {
      return res.status(200).json({
        success: true,
        msg: "The circle info is updated, refresh to reload the new data",
      });
      next();
    } else if ((await result).found >= 1 && (await result).modified == 0) {
      return res.status(500).json({
        success: false,
        msg: "The circle data has been found but not updated.",
      });
      next();
    } else {
      return res
        .status(500)
        .json({ success: false, msg: "The circle has not been found" });
      next();
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      msg: "An error occured in the server, the operation is aborted.",
    });
    next();
  }
}

async function add_element(req, res, next) {
  // First add the member to the members' list of the circle
  // Then add the request to the list of requests of the user

  //They have to be like {_id: "fd9r3jeinff", data: { members: ["kdfdifn", ...]}}
  const circle_id = req.body._id;
  const new_elements = req.body.data;

  const key = Object.keys(new_elements);
  const value = Object.values(new_elements)[0];

  const obj = {}
  obj[key] = { $each : value}

  try {
    const result = update_one(
      circle_id,
      obj,
      "circles",
      "main",
      "$addToSet"
    );
    if ((await result).modified >= 1) {

      // Send notification...

      return res.status(200).json({
        success: true,
        msg: "The new element is added, refresh to reload the new data",
      });
      next();
    } else if ((await result).found >= 1 && (await result).modified == 0) {
      console.log(result);
      return res.status(500).json({
        success: false,
        msg: "The circle's data have been found but the new element has not been added.",
      });
      next();
    } else {
      return res
        .status(500)
        .json({ success: false, msg: "The element has not been found" });
      next();
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      msg: "An error occured in the server, the operation is aborted.",
    });
    next();
  }
}

async function remove_element(req, res, next) {
  // First add the member to the members' list of the circle
  // Then add the request to the list of requests of the user

  //They have to be like {_id: "fd9r3jeinff", data: { members: ["kdfdifn", ...]}}
  const circle_id = req.body._id;
  const new_elements = req.body.data;

  const key = Object.keys(new_elements);
  const value = Object.values(new_elements)[0];

  const obj = {}
  obj[key] = { $in : value}

  try {
    const result = update_one(
      circle_id,
      obj,
      "circles",
      "main",
      "$pull"
    );
    if ((await result).modified >= 1) {

      // Send notification...

      return res.status(200).json({
        success: true,
        msg: "The new element is removed, refresh to reload the new data",
      });
      next();
    } else if ((await result).found >= 1 && (await result).modified == 0) {
      console.log(result);
      return res.status(500).json({
        success: false,
        msg: "The circle's data has been found but the element has not been removed.",
      });
      next();
    } else {
      return res
        .status(500)
        .json({ success: false, msg: "The element has not been found" });
      next();
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      msg: "An error occured in the server, the operation is aborted.",
    });
    next();
  }
}


module.exports = { update_circle, add_element, remove_element };

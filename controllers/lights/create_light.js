const { insert_one } = require("../../services/db_management");
const Light = require("../../objects/light");

async function create_light(req, res, next) {
  const light_info = req.body;

  //Ensuring that the boby is safe...
  const user_info_mandatory = [
    "author",
    "text",
    "status",
    "circles",
    "wallpaper",
  ];
  user_info_mandatory.forEach((prop) => {
    if (!(prop in light_info)) {
      return res.status(406).json({
        success: false,
        msg: "The required user information are not provided.",
      });
    }
  });

  const light = new Light(light_info);

  result = await insert_one(light, "lights", "main");
  if (result ==true) {
      res.status(200).json({success: true, msg: "The new light is successfully created."});
  } else {
      res.status(500).json({success: false, msg: "An error occured in the server."});
  }

  next();
}

module.exports = { create_light };

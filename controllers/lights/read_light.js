const { find_one } = require("../../services/db_management");

async function read_light(req, res, next) {
  const light_id_object = req.body;

  try {
    const existed_light = await find_one(light_id_object, "lights", "main");
    if (existed_light.length !== 0) {
      light_object = {
        author: existed_light.author,
        text: existed_light.text,
        status: existed_light.status,
        circles: existed_light.circles,
        wallpaper: existed_light.wallpaper,
        likes: existed_light.likes,  
        relights: existed_light.relights,
        creation_time: existed_light.creation_time,
      };
      return res.status(200).json({ success: true, data: light_object });
    } else {
      return res
        .status(404)
        .json({ success: false, msg: "The light is not found" });
    }
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
  }
  next();
}

module.exports = { read_light };

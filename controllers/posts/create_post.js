const { insert_one } = require("../../services/db_management");
const Post = require("../../objects/post");

async function create_post(req, res, next) {
  const post_info = req.body;

  //Ensuring that the boby is safe...
  const user_info_mandatory = [
    "author",
    "text",
    "image",
    "publish_time",
    "circles",
  ];
  user_info_mandatory.forEach((prop) => {
    if (!(prop in post_info)) {
      return res.status(406).json({
        success: false,
        msg: "The required user information are not provided.",
      });
    }
  });

  const post = new Post(post_info);

  result = await insert_one(post, "posts", "main");
  if (result ==true) {
      res.status(200).json({success: true, msg: "The new post is successfully created."});
  } else {
      res.status(500).json({success: false, msg: "An error occured in the server."});
  }

  next();
}

module.exports = { create_post };

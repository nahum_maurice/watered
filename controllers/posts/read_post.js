const { find_one } = require("../../services/db_management");

async function read_post(req, res, next) {
  const post_id_object = req.body;

  try {
    const existed_post = await find_one(post_id_object, "posts", "main");
    if (existed_post.length !== 0) {
      post_object = {
        author: existed_post.author,
        text: existed_post.text,
        image: existed_post.image,
        circles: existed_post.circles,
        likes: existed_post.likes,  
        reposts: existed_post.reposts,
        creation_time: existed_post.creation_time,
        publish_time: existed_post.publish_time,
      };
      return res.status(200).json({ success: true, data: post_object });
    } else {
      return res
        .status(404)
        .json({ success: false, msg: "The post is not found" });
    }
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
  }
  next();
}

async function read_post_profile(req, res, next) {
  const post_id_object = req.body;

  try {
    const existed_post = await find_one(post_id_object, "posts", "main");
    if (existed_post.length !== 0) {
      post_object = {
        author: existed_post.author,
        circles: existed_post.circles,
        creation_time: existed_post.creation_time,
      };
      return res.status(200).json({ success: true, data: post_object });
    } else {
      return res
        .status(404)
        .json({ success: false, msg: "The post is not found" });
    }
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
  }
  next();
}

module.exports = { read_post, read_post_profile };

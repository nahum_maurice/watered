const { update_one } = require("../../services/db_management");

async function update_post(req, res, next) {
  // Verify that post has the auth

  //They have to be like {_id: "fd9r3jeinff", data: {password: "fieief"}}
  const post_id_object = req.body._id;
  const new_element = req.body.data;

  try {
    const result = update_one(post_id_object, new_element, "posts", "main");
    if ((await result).modified >= 1) {
      return res.status(200).json({
        success: true,
        msg: "The post info is updated, refresh to reload the new data",
      });
      next();
    } else if ((await result).found >= 1 && (await result).modified == 0) {
      return res.status(500).json({
        success: false,
        msg: "The post data has been found but not updated.",
      });
      next();
    } else {
      return res
        .status(500)
        .json({ success: false, msg: "The post has not been found" });
      next();
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      msg: "An error occured in the server, the operation is aborted.",
    });
    next();
  }
}

module.exports = { update_post };

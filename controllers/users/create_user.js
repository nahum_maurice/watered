const bcrypt = require("bcrypt");

const { insert_one } = require("../../services/db_management");
const User = require("../../objects/user");

async function create_user(req, res, next) {
  const user_info = req.body;

  //Ensuring that the boby is safe...
  const user_info_mandatory = [
    "first_name",
    "last_name",
    "profile_picture_link",
    "bio",
    "username",
    "password",
  ];
  user_info_mandatory.forEach((prop) => {
    if (!(prop in user_info)) {
      return res.status(406).json({
        success: false,
        msg: "The required user information are not provided.",
      });
    }
  })

  // Creating a new user object...
  const user = new User(user_info);

  // Hashing of the password using bcrypt...
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  // Adding the new created user to the DB (using DB management service)
  result = await insert_one(user)
  if (result==true) {
    res
      .status(200)
      .json({ success: true, msg: "The new user is successfully created" });
  } else {
    res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
  }

  //Create and return a new token for the user authorization...
  
  next();
}

module.exports = { create_user };

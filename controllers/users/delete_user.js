const { delete_one } = require("../../services/db_management");

async function delete_user(req, res, next) {
  //  Verify that the users has the auth

  const user_id_object = req.body;

  try {
    const result = delete_one(user_id_object);
    if ((await result).deleted >= 1) {
      return res
        .status(200)
        .json({
          success: true,
          msg: "The user has been deleted successfully.",
        });
      next();
    } else if ((await result).found >= 1 && (await result).deleted == 0) {
        return res.status(500).json({
            success: false,
            msg: "The user data has been found but not deleted.",
          });
        next();
    } else {
      return res
        .status(500)
        .json({ success: false, msg: "The user has not been found." });
      next();
    }
  } catch (error) {
    return res
      .status(500)
      .json({
        succes: false,
        msg: "An error occured in the server, the operation is aborted.",
      });
    next();
  }
}

module.exports = { delete_user };

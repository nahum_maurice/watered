const { find_one } = require("../../services/db_management");

async function read_user(req, res, next) {
  // Verify that the user has the right (authorization)

  // Verify the existence of the user in the database
  const user_id_object = req.body; //Should be an object

  try {
    const existed_user = await find_one(user_id_object);
    if (existed_user.length !== 0) {
      user_object = {
        first_name: existed_user.first_name,
        last_name: existed_user.last_name,
        profile_picture_link: existed_user.profile_picture_link,
        username: existed_user.username,
        suspended: existed_user.suspended,
        circles: existed_user.circles,
        connected: existed_user.connected,
        corner: existed_user.corner,
        requests: existed_user.requests,
      };
      return res.status(200).json({ success: true, data: user_object });
      next();
    } else {
      return res
        .status(404)
        .json({ success: false, msg: "The user is not found" });
      next();
    }
  } catch (error) {
    return res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
    next();
  }
}

async function read_user_profile(req, res, next) {
  //Verify where or not the user has the authorisation...

  const user_id_object = req.body; //Should be an object

  try {
    const existed_user = await find_one(user_id_object);
    if (existed_user.length !== 0) {
      user_object = {
        first_name: existed_user.first_name,
        last_name: existed_user.last_name,
        profile_picture_link: existed_user.profile_picture_link,
        username: existed_user.username,
        suspended: existed_user.suspended,
        circles: existed_user.circles,
        connected: existed_user.connected,
      };
      return res.status(200).json({ success: true, data: user_object });
      next();
    } else {
      return res
        .status(404)
        .json({ success: false, msg: "The user is not found" });
      next();
    }
  } catch (error) {
    return res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
    next();
  }
}

async function read_user_partial_profile(req, res, next) {
  //  Verify that the user has the authorization...

  const user_id_object = req.body; //Should be an object

  try {
    const existed_user = await find_one(user_id_object);
    if (existed_user.length !== 0) {
      user_object = {
        first_name: existed_user.first_name,
        last_name: existed_user.last_name,
        profile_picture_link: existed_user.profile_picture_link,
        username: existed_user.username,
        suspended: existed_user.suspended,
      };
      return res.status(200).json({ success: true, data: user_object });
      next();
    } else {
      return res
        .status(404)
        .json({ success: false, msg: "The user is not found" });
      next();
    }
  } catch (error) {
    return res
      .status(500)
      .json({ success: false, msg: "An error occured in the server" });
    next();
  }
}

module.exports = { read_user, read_user_profile, read_user_partial_profile };

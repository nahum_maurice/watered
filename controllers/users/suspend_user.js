const { update_one } = require("../../services/db_management");

async function suspend_user(req, res, next) {
  //Verify that the user has the admin right.

  const user_id_object = req.body._id;

  try {
    const result = await update_one(user_id_object, { suspended: true });
    if (result.modified >= 1) {
      return res.status(200).json({
        success: true,
        msg: "The user has been suspended, refresh to reload the new data",
      });
      next();
    } else if (result.found >= 1 && result.modified == 0) {
        return res.status(500).json({success: false, msg: "The user data has been found but not suspended"});
        next();
    } else {
      return res.status(500).json({
        success: false,
        msg: "The user has not been found."
      });
      next();
    }
  } catch (error) {
    return res
      .status(500)
      .json({ success: false, msg: "An error occured in the server." });
    next();
  }
}

async function unsuspend_user(req, res, next) {
    //Verify that the user has the admin right.
  
    const user_id_object = req.body._id;
  
    try {
      const result = await update_one(user_id_object, { suspended: false });
      if (result.modified >= 1) {
        return res.status(200).json({
          success: true,
          msg: "The user has been unsuspended, refresh to reload the new data",
        });
        next();
      } else if (result.found >= 1 && result.modified == 0) {
          return res.status(500).json({success: false, msg: "The user data has been found but not unsuspended"});
          next();
      } else {
        return res.status(500).json({
          success: false,
          msg: "The user has not been found."
        });
        next();
      }
    } catch (error) {
      return res
        .status(500)
        .json({ success: false, msg: "An error occured in the server." });
      next();
    }
  }
  

module.exports = { suspend_user, unsuspend_user };

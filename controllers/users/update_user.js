const bcrypt = require("bcrypt");

const { update_one } = require("../../services/db_management");

async function update_user(req, res, next) {
  // Verify that user has the auth

  //They have to be like {_id: "fd9r3jeinff", data: {password: "fieief"}}
  const user_id_object = req.body._id;
  const new_element = req.body.data;

  //Encrypt the new password if it is in the doc
  if ("password" in new_element) {
    const salt = await bcrypt.genSalt(10);
    new_element.password = await bcrypt.hash(new_element.password, salt);
  }

  try {
    const result = update_one(user_id_object, new_element);
    if ((await result).modified >= 1) {
      return res.status(200).json({
        success: true,
        msg: "The user info is updated, refresh to reload the new data",
      });
      next();
    } else if ((await result).found >= 1 && (await result).modified == 0) {
      return res.status(500).json({
        success: false,
        msg: "The user data has been found but not updated.",
      });
      next();
    } else {
      return res
        .status(500)
        .json({ success: false, msg: "The user has not been found" });
      next();
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      msg: "An error occured in the server, the operation is aborted.",
    });
    next();
  }
}

module.exports = { update_user };

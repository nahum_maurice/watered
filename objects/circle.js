const { v4: uuidv4 } = require("uuid");

class Circle {
    constructor (circle_object) {
        this.name = circle_object.name;
        this.description = circle_object.description;
        this.creator = circle_object.creator;
        this.isPrivate = circle_object.isPrivate;
        
        // on-server made components
        this.members = [circle_object.creator]; // Initializing with a one member group
        this._id = uuidv4();
        this.posts = []; // Will contain all the circle's posts & channels...
        this.creation_time = new Date().toLocaleString;
    }
}

module.exports = Circle;

const { v4: uuidv4 } = require("uuid");

class Light {
  constructor(light_object) {
    this.author = light_object.author;
    this.text = light_object.text;
    this.status = light_object.status;
    this.circles = light_object.circles;
    this.wallpaper = light_object.wallpaper;

    // Adding on-server created elements
    this.likes = 0;
    this.relight = 0;
    this.creation_time = new Date().toLocaleString;
    this._id = uuidv4();
  }
}

module.exports = Light;

const { v4: uuidv4 } = require("uuid");

class Post {
    constructor(post_object) {
      this.author = post_object.author;
      this.text = post_object.text;
      this.image = post_object.image;
      this.circles = post_object.circles;
      this.publish_time = post_object.publish_time;
  
      //  Adding on-server created elements
      this.likes = 0;
      this.reposts = 0;
      this.creation_time = new Date().toLocaleString;
      this._id = uuidv4();
    }
  }

  module.exports = Post;
const { v4: uuidv4 } = require('uuid');

class User {
  constructor(user_object) {
    this.first_name = user_object.first_name;
    this.last_name = user_object.last_name;
    this.profile_picture_link = user_object.profile_picture_link;
    this.bio = user_object.bio;
    this.username = user_object.username;
    this.password = user_object.password;
    this._id = uuidv4();
    this.suspended = false;

    //Adding the user data...
    this.circles = [];
    this.connected = []; //  The list of people the user is following
    this.corner = [] //  The saved contents of the user.
    this.docs = [] //  Content the user's produced texts.
    this.requests = [] // Contents the requests such as new group, new connection...
  };
}

module.exports = User;
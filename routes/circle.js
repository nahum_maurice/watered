const express = require("express");
const router = express.Router();

// Middlewares
const { create_circle } = require("../controllers/circles/create_circle");
const {
  read_circle,
  read_circle_profile,
  read_circle_partial_profile,
} = require("../controllers/circles/read_circle");
const {
  update_circle,
  add_element,
  remove_element,
} = require("../controllers/circles/update_circle");
const { delete_circle } = require("../controllers/circles/delete_circle");

router.get("/", (req, res) => {
  // Should return the documentation part for circles...
  res.status(200).json({ success: true, data: "Hello world" });
});

router.post("/create-circle", create_circle);
router.get("/get-circle", read_circle);
router.get("/get-circle-profile", read_circle_profile);
router.get("/get-circle-partial-profile", read_circle_partial_profile);
router.put("/update-circle", update_circle);
router.delete("/delete-circle", delete_circle);
router.put("/add-member", add_element);
router.put("/remove-member", remove_element);
router.put("/add-post", add_element);
router.put("/remove-post", remove_element);

module.exports = router;

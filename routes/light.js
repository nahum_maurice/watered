const express = require("express");
const router = express.Router();

// Middlewares
const { read_light } = require("../controllers/lights/read_light");
const { create_light } = require("../controllers/lights/create_light");
const { delete_light } = require("../controllers/lights/delete_light");

// The routes...
router.get("/", (req, res) => {
  // Should return the documentation part for lights...
  res.status(200).json({ success: true, data: "Hello world" });
});

router.post("/create-light", create_light);
router.get("/get-light", read_light);
router.delete("/delete-light", delete_light);

module.exports = router;

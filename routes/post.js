const express = require("express");
const router = express.Router();

// Middlewares
const { read_post, read_post_profile } = require("../controllers/posts/read_post");
const { create_post } = require("../controllers/posts/create_post");
const { update_post } = require("../controllers/posts/update_post");
const { delete_post } = require("../controllers/posts/delete_post");

// The routes...
router.get("/", (req, res) => {
  // Should return the documentation part for posts...
  res.status(200).json({ success: true, data: "Hello world" });
});

router.post("/create-post", create_post);
router.get("/get-post", read_post);
router.get("/get-post-profile", read_post_profile);
router.put("/update-post", update_post);
router.delete("/delete-post", delete_post);

module.exports = router;

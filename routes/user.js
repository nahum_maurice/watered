const express = require("express");
const router = express.Router();

// Middlewares
const { create_user } = require("../controllers/users/create_user");
const { read_user, read_user_profile, read_user_partial_profile } = require("../controllers/users/read_user");
const { update_user } = require("../controllers/users/update_user")
const { delete_user } = require("../controllers/users/delete_user")
const { suspend_user, unsuspend_user } = require("../controllers/users/suspend_user")

router.get("/", (req, res) => {
  // Should return the documentation part for users...
  res.status(200).json({ success: true, data: "Hello world" });
});

router.post("/create-user", create_user);
router.get("/get-user", read_user);
router.get("/get-user-profile", read_user_profile);
router.get("/get-user-partial-profile", read_user_partial_profile);
router.put("/update-user", update_user);
router.delete("/delete-user", delete_user);
router.put("/suspend-user", suspend_user);
router.put("/unsuspend-user", unsuspend_user);

module.exports = router;

//const database = require("mime-db");
const { MongoClient } = require("mongodb");

const uri =
  "mongodb+srv://nahum:bRab9DpTt-wwE5L@cluster0.mipui.mongodb.net/watered?retryWrites=true&w=majority";

const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

async function db_connection() {
  await client.connect((e) => {
    if (e) {
      console.log(e);
    } else {
      console.log("Databases connected...");
    }
  });
}

async function find_one(object, database = "users", collection = "main") {
  const db = client.db(database);
  const col = db.collection(collection);

  let result = {};

  await col.findOne(object).then((res) => {
    result = res;
  });

  return result;
}

function insert_one(object, database = "users", collection = "main") {
  const db = client.db(database);
  const col = db.collection(collection);

  var res = true;

  col.insertOne(object, (err) => {
    if (err) {
      res = false;
    }
  });

  return res;
}

async function update_one(
  object_id,
  object,
  database = "users",
  collection = "main",
  action = "$set"
) {
  const db = client.db(database);
  const col = db.collection(collection);

  let result = {
    found: 0,
    modified: 0,
  };

  if (action == "$set") {
    await col.updateOne({ _id: object_id }, { $set: object }).then((res) => {
      result.found = res.result.n;
      result.modified = res.modifiedCount;
    });
  } else if (action == "$inc") {
    await col.updateOne({ _id: object_id }, { $inc: object }).then((res) => {
      result.found = res.result.n;
      result.modified = res.modifiedCount;
    });
  } else if (action == "$addToSet") {
    await col.updateOne({ _id: object_id }, { $addToSet: object }).then((res) => {
      result.found = res.result.n;
      result.modified = res.modifiedCount;
    });
  } else if (action == "$pull") {
    await col.updateOne({ _id: object_id }, { $pull: object }).then((res) => {
      result.found = res.result.n;
      result.modified = res.modifiedCount;
    });
  }

  return result;
}



async function delete_one(object_id, database = "users", collection = "main") {
  const db = client.db(database);
  const col = db.collection(collection);

  let result = {
    found: 0,
    deleted: 0,
  };

  await col.deleteOne(object_id).then((res) => {
    result.found = res.result.n;
    result.deleted = res.deletedCount;
  });

  return result;
}

module.exports = {
  db_connection,
  find_one,
  insert_one,
  update_one,
  delete_one,
};
